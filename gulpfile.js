var fs      = require('fs'),
    gulp    = require('gulp'),
    fest    = require('fest');


function festCompile(name) {
  var src       = './dist/javascripts/fest/' + name + '.xml',
      compiled  = fest.compile(src, {beautify: false});

  var tpl = ";(function (getNs) {" +
            "getNs('fest.gallery')['" + name + "'] = " + compiled +
            "})(this.getNameSpace);";
  
  fs.writeFile("dist/javascripts/fest/" + name + ".js", tpl); 
}


gulp.task('fest', function () {
  festCompile('gallery');
})

gulp.task('default', ['fest']);
