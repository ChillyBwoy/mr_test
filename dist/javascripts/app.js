(function ($, window, oCpf) {
  'use strict';

  var basicTools    = oCpf.Basic;
  var GalleryModel  = oCpf.Modules.Gallery;


  var getGalleryData = function () {
    // Для простоты все данные положим в localStorage (javascripts/fixtures/gallery.js)

    var rawData = JSON.parse(window.localStorage.getItem('gallery'));

    return {
      name:   rawData.item.name,
      images: rawData.list.map(function (imageData) {
        return {
          url: [
            'http://' + imageData.img_190x150,
            'http://' + imageData.img_1280
          ]
        };
      })
    };
  };


  var toggleEl = function(el, hide) {
    var v = hide ? 'none' : 'block';
    return el.css('display', v);
  };

  var clearList = function(list) {
    return list.find('li').removeClass('gallery-thumbs_list-el__selected');
  };

  var GalleryThumbsView = basicTools.Constructors.getView({
    // непонятно где объявлять события
    _Events: ['thumbsMove'],
    _Handlers: {

      self: {
        thumbsMove: function(value) {

          var size    = this._getImages().length,
              perPage = this._getThumbsPerPage();
      
          // нужно для того, чтобы список не уходил "слишком влево/вправо"
          value = value < 0 ? 0 : value;
          value = (size - value) < perPage ? (size - perPage) : value;

          this._thumbsOffset = value;
          this._toggleArrows();

          this._Elems.thumbsList.css({
            'left': '-' + (this._thumbWidth * value) + '%'
          });
        },
      },

      model: {
        imageready: function (imageObject, imageId, setId, versionId) {
          // при загрузке каждого изображения
          var elemsObject = this._Elems;
          if (versionId === 1) {
            elemsObject.viewport.css('backgroundImage', 'url(\'' + imageObject.versions[versionId].src + '\')');
            elemsObject.Parent.removeClass(this._Opts.cssClss.loading);
          }

          this._setState('ready');

          clearList(this._Elems.thumbsList);

          var current = this._Model._Current,
              thumb   = this._Elems.thumbs.find('li').get(current.image);

          $(thumb).addClass('gallery-thumbs_list-el__selected');
        },

        imagechange: function (/* imageObject, imageId, setId */) {
          this._Elems.Parent.addClass(this._Opts.cssClss.loading);
          this._setState('loading');
          this._toggleArrows();
        }
      },

      dom: {
        'click:Main.prev': function() {
          if (this._Opts.state == 'ready') {
            this._Model.prevImage();
            this._checkAndMove(this._Model._Current.image);
          }
        },

        'click:Main.next': function() {
          //проверяем закончилась ли загрузка выбранного изображения
          if (this._Opts.state == 'ready') {
            this._Model.nextImage();
            this._checkAndMove(this._Model._Current.image);
          }
        },

        'click:Main.thumbsPrev': function() {
          this._checkAndMove(this._thumbsOffset - 1);
        },

        'click:Main.thumbsNext': function() {
          this._checkAndMove(this._thumbsOffset + 1);
        },

        'click:Main.thumbsItems': function(event) {
          var node      = $(event.target),
              position  = node.index();
          this._Model.changeImage(position);
        }
      }
    },

    _getImages: function () {
      return this._Model._getCurrentSetImages();
    },

    _setState: function(state) {
      this._Opts.state = state;
    },

    _checkAndMove: function(value) {
      this._trigger('thumbsMove', value);
    },

    _renderTemplate: function(context) {
      var tpl  = this._Opts.templates.main;
      this._Elems.node.html(tpl(context));
    },

    _render: function() {
      var props = this._Model.getSetParams(0);

      // готовим изображения для превьюшек
      var images = (this._getImages() || []).map(function(img) {
        return img.versions[0].src;
      });

      this._renderTemplate({
        name:     props.name,
        images:   images,
        current:  this._Model._Current.image,
        total:    images.length
      });

      this._getElements(this._Opts.cssSels.Main);
      this._setThumbsWidth();
    },

    _setThumbsWidth: function() {
      this._Elems.thumbsItems.css('width', this._thumbWidth + '%');
    },

    _toggleArrows: function() {
      var current = this._Model._Current,
          images  = this._getImages(),
          perPage = this._getThumbsPerPage();

      if (images.length <= perPage) {
        toggleEl(this._Elems.thumbsPrev, true);
        toggleEl(this._Elems.thumbsNext, true);
      } else {
        toggleEl(this._Elems.thumbsPrev, this._thumbsOffset <= 0);
        toggleEl(this._Elems.thumbsNext, this._thumbsOffset >= images.length - perPage);
      }

      if (!this._Model._Opts.Loop) {
        // если первая/последняя в списке, то убираем/показываем
        // соответствующую стрелку
        toggleEl(this._Elems.prev, current.image === 0);
        toggleEl(this._Elems.next, current.image >= images.length - 1);
      }
    },

    _getThumbsPerPage: function() {
      var images  = this._getImages(),
          perPage = this._Opts.thumbsPerPage;

      return perPage > images.length ? images.length : perPage;
    },

    _Init: function () {
      this._Model.changeSet(0);

      this._thumbWidth    = 100 / this._getThumbsPerPage();
      this._thumbsOffset  = 0;


      this._render();
      this._Model.changeImage(0);
    }

  }, {
    cssSels: {
      Main: {
        node:         '.gallery_in',
        viewport:     '.gallery-viewport',
        thumbs:       '.gallery-thumbs',
        thumbsList:   '.gallery-thumbs_list',
        thumbsItems:  '.gallery-thumbs_list-el',
        prev:         '.gallery-viewport_nav-url__prev',
        next:         '.gallery-viewport_nav-url__next',
        thumbsPrev:   '.gallery-thumbs_nav-url__prev',
        thumbsNext:   '.gallery-thumbs_nav-url__next'
      }
    },
    cssClss: {
      loading: 'gallery__loading'
    },
    templates: {
      main: window.getNameSpace('fest.gallery').gallery
    },
    state: 'idle',
    thumbsPerPage: 8 //количество превью
  }, null, 'galleryThumbsView');


  var data = getGalleryData();
  var gallery = new GalleryModel({Loop: true}, data);

  new GalleryThumbsView({thumbsPerPage: 10}, $('.gallery'), gallery);

}(window.jQuery, window, window.ru.mail.cpf));
