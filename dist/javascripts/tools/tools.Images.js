/**
 * @require 'lib/basic.js'
 * @require 'tools.Deferred.js'
 */
(function (window, oNS) {
	var oBasic = oNS.Basic;
	var typeOf = oBasic.typeOf;
	var fExtend = oBasic.Extend;
	var fDeferred = oNS.Comp.Tools.Deferred;
	var reFileExt = /\.([a-z]{3,4})$/i;
	var rePattern = /[^a-z]/i;
	var oImageTypesParams = {
		flash: {
			exts: ['swf'],
			canPreload: false
		},
		pic : {
			exts: ['jpe?g', 'png', 'gif'],
			canPreload: true
		}
	};
	function getFileExt(sFileName) {
		var mExt = sFileName.match(reFileExt);
		return mExt !== null && mExt[1];
	}
	function getImageTypeFromUrl(sUrl) {
		var sUrlExt = getFileExt(sUrl);
		var oTypeParams, aTypeExts, mCurExt, sType = null;
		if (sUrlExt) {
			for (var typeName in oImageTypesParams) {
				if (oImageTypesParams.hasOwnProperty(typeName)) {
					oTypeParams = oImageTypesParams[typeName];
					aTypeExts = oTypeParams.exts;
					for (var extNo = 0; extNo < aTypeExts.length; extNo++) {
						if (typeOf(mCurExt = aTypeExts[extNo], 'string') && rePattern.test(mCurExt)) {
							aTypeExts[extNo] = mCurExt = new RegExp('^' + mCurExt + '$', 'i');
						}
						if (typeOf(mCurExt, 'string') ? mCurExt === sUrlExt : mCurExt.test(sUrlExt)) {
							sType = typeName;
							break;
						}
					}
				}
			}
		}
		return sType;
	}
	function preloadImage(sImgUrl, iFailTimeOut) {
		var failTimerId, elImg, oDeferred, fOnFail;
		iFailTimeOut = !isNaN(iFailTimeOut) && iFailTimeOut !== null ? iFailTimeOut : 5e3;
		oDeferred = fDeferred();
		fOnFail = function () {
			clearTimeout(failTimerId);
			elImg.onload = elImg.onerror = null;
			oDeferred.reject();
		};
		elImg = new Image();
		elImg.onload = function () {
			clearTimeout(failTimerId);
			oDeferred.resolve(elImg);
		};
		elImg.onerror = fOnFail;
		failTimerId = setTimeout(fOnFail, iFailTimeOut);
		elImg.src = sImgUrl;
		return oDeferred.promise();
	}
	function isTypeCorrect(sImageType) {
		return sImageType in oImageTypesParams;
	}
	fExtend(window.getNameSpace('Tools.Images', oNS), {
		preloadImage: preloadImage,
		canPreload: function (sImageType) {
			return isTypeCorrect(sImageType) && oImageTypesParams[sImageType].canPreload === true;
		},
		isTypeCorrect: isTypeCorrect,
		getImageTypeFromUrl: getImageTypeFromUrl,
		_addTypes: function (oTypes) {
			var oTypeParams;
			for (var typeName in oTypes) {
				if (oTypes.hasOwnProperty(typeName)) {
					oTypeParams = oTypes[typeName];
					if (typeOf(oTypeParams, 'object') && oTypeParams) {
						oImageTypesParams[typeName] = fExtend(true, {}, oTypeParams);
					}
				}
			}
		}
	});
}(this, this.ru.mail.cpf));