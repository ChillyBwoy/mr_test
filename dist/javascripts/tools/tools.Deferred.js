/**
 *  @requires 'lib/basic.js'
 *  @requires 'lib/polyfills/es5.basic.js'
 *  @requires 'tools/tools.Callbacks.js'
 */
(function (window, oCpf) {
	var oTools = oCpf.Tools;
	var fCallbacks = oTools.Callbacks;
	var oBasic = oCpf.Basic;
	var typeOf = oBasic.typeOf;
	var fExtend = oBasic.Extend;
	var createPromise = (function () {
		var aTypes = ['resolve', 'reject'];
		var statesList = ['rejected', 'resolved', 'pending'];
		var getHandlerType = function (resolve) {
			return typeOf(resolve, 'boolean') ? aTypes[resolve ? 0 : 1] : aTypes.join(' ');
		};
		function addHandler(isResolveHandler, fHandler) {
			if (typeOf(fHandler, 'function')) {
				this.add(getHandlerType(isResolveHandler), fHandler);
			}
		}
		function updateState(stateArguments, resolve) {
			if (!typeOf(resolve, 'boolean')) {
				return;
			}
			this.fire(getHandlerType(resolve), stateArguments);
			this._disable();
		}
		function wrapHandler(resultPromise, fHandler) {
			return function () {
				var mResult;
				try {
					mResult = fHandler.apply(this, arguments);
				} catch (oEx) {
					resultPromise.reject(oEx);
					return;
				}
				if (typeOf(mResult, 'object') && typeOf(mResult.then, 'function')) {
					mResult.then(resultPromise.resolve, resultPromise.reject);
				} else {
					resultPromise.resolve(mResult);
				}
			};
		}
		return function (fResolver) {
			var callbacksInstance = fCallbacks({Types: aTypes, Memory: true, Once: true});
			var currentState = statesList[2];
			var promiseObject, deferredObject;
			promiseObject = {};
			function setState(deferredObject, resolve) {
				updateState.call(this, Array.prototype.slice.call(arguments, 2), resolve);
				currentState = statesList[resolve ? 1 : 0];
				return deferredObject;
			}
			function addPromiseHandler(resolve, fHandler) {
				addHandler.call(this, resolve, fHandler);
				return promiseObject;
			}
			promiseObject = fExtend(promiseObject, {
				done: addPromiseHandler.bind(callbacksInstance, true),
				fail: addPromiseHandler.bind(callbacksInstance, false),
				then: function () {
					var resultPromise = createPromise();
					var fHandler, fWrapper, isFulfillHandler;
					for (var argNo = 0; argNo < 2; argNo++) {
						isFulfillHandler = argNo === 0;
						fHandler = arguments[argNo];
						if (typeOf(fHandler, 'function')) {
							fWrapper = wrapHandler(resultPromise, fHandler);
						} else {
							fWrapper = resultPromise[isFulfillHandler ? 'resolve' : 'reject'];
						}
						addHandler.call(callbacksInstance, isFulfillHandler, fWrapper);
					}
					return resultPromise;
				},
				always: addHandler.bind(callbacksInstance, null),
				state: function () {
					return currentState;
				},
				promise: function (promiseTarget) {
					if (!typeOf(promiseTarget, 'object')) {
						promiseTarget = promiseObject;
					} else {
						for (var methodName in promiseObject) {
							if (promiseObject.hasOwnProperty(methodName)) {
								promiseTarget[methodName] = promiseObject[methodName];
							}
						}
					}
					return promiseTarget;
				},
				progress: function () {} //XXX jquery Deferred.when compatibility
			});
			if (typeOf(fResolver, 'function')) { //es6 Promise like interface
				fResolver.call(
					promiseObject,
					setState.bind(callbacksInstance, null, true),
					setState.bind(callbacksInstance, null, false)
				);
			} else { //jquery Deferred like interface
				deferredObject = promiseObject.promise({});
				fExtend(true, deferredObject, {
					resolve: setState.bind(callbacksInstance, deferredObject, true),
					reject: setState.bind(callbacksInstance, deferredObject, false),
					destroy: function () {
						callbacksInstance._destroy();
					}
				});
			}
			return deferredObject || promiseObject;
		};
	})();
	var createMultiplePromise = (function () {
		function hOnDone() {
			var aArgs = Array.prototype.slice.call(arguments);
			var aResolveParams = aArgs.splice(0, 2);
			var aResolveArgs = aResolveParams[0];
			aResolveArgs[aResolveParams[1]] = aArgs[0];
			if (--this.length === 0) {
				this.promise.resolve.apply(null, aResolveArgs);
			}
		}
		function hOnFail() {
			this.promise.reject();
		}
		return function () {
			var resultPromise = createPromise();
			var oState = {length: arguments.length, promise: resultPromise};
			var aResArgs = [], mArg;
			for (var argNo = oState.length; argNo--;) {
				mArg = arguments[argNo];
				if (mArg && mArg.done && typeOf(mArg.done, 'function')) { //TODO thenable
					mArg.done(hOnDone.bind(oState, aResArgs, argNo)).fail(hOnFail.bind(oState));
				} else {
					aResArgs[argNo] = mArg;
					oState.length--;
				}
			}
			if (oState.length === 0) {
				resultPromise.resolve.apply(null, aResArgs);
			}
			return resultPromise; //TODO нужен ли именно promise?
		};
	}());

	createPromise.all = function (promiseList) {
		return createPromise(function (resolve, reject) {
			if (!Array.isArray(promiseList)) {
				return reject(new TypeError('Invalid argument type'));
			}
			var counter = promiseList.length;
			var resolvedWith = [];
			for (var promiseNo = 0; promiseNo < promiseList.length; promiseNo++) {
				try {
					promiseList[promiseNo].then(function (result) {
						resolvedWith[promiseNo] = result;
						if (--counter === 0) {
							resolve(resolvedWith);
						}
					}, reject);
				} catch (exeption) {
					return (new TypeError('Invalid argument type'));
				}
			}
		});
	};

	function createPromiseWithState(resolve, value) {
		return createPromise(function () {
			arguments[resolve ? 0 : 1](value);
		});
	}

	createPromise.reject = createPromiseWithState.bind(null, true);
	createPromise.reject = createPromiseWithState.bind(null, false);

	createPromise.when = createMultiplePromise;
	var oExports = {
		Promise: createPromise,
		/** @deprecated */
		Deferred: createPromise
	};
	fExtend(true, oTools, oExports);
	/** @deprecated */
	fExtend(true, window.getNameSpace('Comp.Tools', oCpf), oExports);
}(this, this.ru.mail.cpf));