/**
 * Виджет календаря
 * @requires 'lib/types/types.Date.js'
 */
(function ($, window, oCpf) {
  var oBasic = oCpf.Basic;
  var typeOf = oBasic.typeOf;
  var oDates = oCpf.Types.Date;
  var defOptions = {
    cssSels: {
      Main: {
        datesCont: '.js-calendar__dates',
        monthVal: '.js-calendar__month-val',
        yearVal: '.js-calendar__year-val'
      },
      dateElem: '.js-calendar__date',
      nextMonth: '.js-calendar__next-month',
      prevMonth: '.js-calendar__prev-month'
    },
    cssClss: {
      selectedDate: 'calendar__cell_selected'
    },
    datesRange : {
      min: '2000-01',
      max: null,
      loop: false
    },
    selectionLimits: {
      min: null,
      max: 1,
      fifoSwitch: true
    },
    names: {
      months : ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']/*,
      days : ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']*/
    },
    templates: {
      table: null
    },
    today: new Date(),
    startMonth: null,
    minWeekRows: null
  };
  var Calendar = oBasic.Constructors.getView((function () {
    var dayStartFormat = '5h0i0s0n';
    var monthIdFormat = 'YYYYmm';
    function getDate(mVal) {
      var dVal = mVal;
      if (mVal !== null) {
        dVal = oDates.getDate(mVal);
        if (dVal !== null) {
          dVal = oDates.Clone(dVal);
        }
      }
      return dVal;
    }
    function isInList(oList, aDate) {
      var monthAvailable = oList[aDate[0]];
      return Array.isArray(monthAvailable) && monthAvailable.indexOf(aDate[1]) > -1;
    }
    function isAvailableDate(dateToСheck) {
      var availableLists = this._lists;
      var dateArray = [oDates.toStr(dateToСheck, monthIdFormat), dateToСheck.getDate()];
      return (availableLists.available === null || isInList(availableLists.available, dateArray)) &&
        (availableLists.disabled === null || !isInList(availableLists.disabled, dateArray));
    }
    function getDateIndex(index, curValue, currentIdx) {
      if (index < 0 && oDates.Compare(curValue, this, 'd') === 0) {
        index = currentIdx;
      }
      return index;
    }
    function isSelectedDate(dateToСheck) {
      return this._current.selection.reduce(getDateIndex.bind(dateToСheck), -1) > -1;
    }
    function getMonthDatesArray(dMonth) {
      var options = this._Opts;
      var minimumWeeks = options.minWeekRows;
      var dToday = this._today;
      var datesRange = this._datesRange;
      var targetMonth = dMonth.getMonth();
      var currentDate = oDates.Clone(dMonth, '1d' + dayStartFormat);
      var hasWeeksLimit = minimumWeeks !== null && !isNaN(minimumWeeks);
      var aResult = [], aWeekDays = [];
      var firstMonthDay = currentDate.getDay() - 1;
      var weekDay = 0, weeksCount = 0, dateInCurrentMonth, aDateStrings;
      aResult.push(aWeekDays);
      if (firstMonthDay > 0) {
        oDates.Change(currentDate, '-' + firstMonthDay + 'd');
      }
      dateInCurrentMonth = currentDate.getMonth() === targetMonth;
      while (weekDay < 7) {
        aDateStrings = oDates.toStr(currentDate, 'YYYY-MM-dd').split('-');
        aWeekDays.push({
          text: currentDate.getDate(),
          strDate: {
            date: aDateStrings[2],
            month: aDateStrings[1],
            year: aDateStrings[0],
            monthName: oDates.toStr(currentDate, 'MM', options.names.months)
          },
          currentMonth: dateInCurrentMonth,
          disabled: !(oDates.isInRange(currentDate, datesRange) && isAvailableDate.call(this, currentDate)),
          selected: isSelectedDate.call(this, currentDate),
          today: oDates.Compare(currentDate, dToday, 'd') === 0
        });
        oDates.Change(currentDate, '+1d');
        dateInCurrentMonth = currentDate.getMonth() === targetMonth;
        weekDay++;
        if (weekDay === 7 && (dateInCurrentMonth || (hasWeeksLimit && weeksCount < minimumWeeks))) {
          weeksCount++;
          weekDay = 0;
          aResult.push(aWeekDays = []);
        }
      }
      return aResult;
    }
    function drawDates() {
      var options = this._Opts;
      var oCurrent = this._current;
      //var aWeekDayNames = options.names.days;
      var fTableTamplate = options.templates.table;
      var aMonthDates = getMonthDatesArray.call(this, oCurrent.month);
      //oCurrent.monthDates = aMonthDates;
      this._Elems.datesCont.html(
        fTableTamplate({
          type: 'dates',
          data: aMonthDates
        })
      );
    }
    function fillMonthVals() {
      var elements = this._Elems;
      var dCurrentMonth = this._current.month;
      elements.monthVal.text(oDates.toStr(dCurrentMonth, 'MM', this._Opts.names.months));
      elements.yearVal.text(oDates.toStr(dCurrentMonth, 'YYYY'));
    }
    function getRange() {
      var rangeOption = this._Opts.datesRange;
      this._datesRange = oDates.getRange({
        min: rangeOption.min,
        max: rangeOption.max
      });
    }
    function getElemByDate(dDate) {
      return this._Elems.datesCont.find(this._Opts.cssSels.dateElem).eq(dDate.getDate() - 1);
    }
    function getDateByElem(elDate) {
      var elementIndex = this._Elems.datesCont.find(this._Opts.cssSels.dateElem).index(elDate);
      if (elementIndex > -1) {
        return oDates.Clone(this._current.month, (elementIndex + 1) + 'd');
      }
    }
    function deselectDate(dSelect) {
      var options = this._Opts;
      var currentVals = this._current;
      var aSelection = currentVals.selection;
      var selectionLimits = options.selectionLimits;
      var minSelections = selectionLimits.min;
      var selectedIndex = aSelection.reduce(getDateIndex.bind(dSelect), -1);
      var isSelected = selectedIndex > -1;
      if (isSelected) {
        if (minSelections !== null && minSelections >= aSelection.length) {
          return;
        } else {
          aSelection.splice(selectedIndex, 1);
          this._trigger('selectionChange', aSelection.slice(0));
        }
      }
      if (oDates.Compare(dSelect, currentVals.month, 'm') === 0) {
        getElemByDate.call(this, dSelect).removeClass(options.cssClss.selectedDate);
      }
    }
    function selectDate(dSelect, triggerChange) {
      var options = this._Opts;
      var currentVals = this._current;
      var aSelection = currentVals.selection;
      var selectionLimits = options.selectionLimits;
      var maxSelections = selectionLimits.max;
      var useFifoSwitch = selectionLimits.fifoSwitch;
      var useSwitch = typeOf(useFifoSwitch, 'boolean');
      if (!isAvailableDate.call(this, dSelect)) {
        return;
      }
      if (maxSelections !== null && aSelection.length >= maxSelections) {
        if (useSwitch) {
          deselectDate.call(this, aSelection[useFifoSwitch ? 'shift' : 'pop']());
        } else {
          return;
        }
      }
      aSelection.push(dSelect);
      if (triggerChange !== false) {
        this._trigger('selectionChange', aSelection.slice(0));
      }
      if (oDates.Compare(dSelect, currentVals.month, 'm') === 0) {
        getElemByDate.call(this, dSelect).addClass(options.cssClss.selectedDate);
      }
    }
    function getLists() {
      //TODO получить из аргументов (?)
      this._lists = {
        available: null,
        disabled: null
      };
    }
    function switchMonth(switchForward) {
      this.setMonth(oDates.Clone(this._current.month, (switchForward ? '+1' : '-1') + 'm'));
    }
    return {
      _Events: ['selectionChange'],
      _Handlers: {
        dom: {
          'click:dateElem': function (oEvt) {
            this.toggleDateSelection(getDateByElem.call(this, oEvt.currentTarget));
            oEvt.preventDefault();
          },
          'click:nextMonth': function (oEvt) {
            switchMonth.call(this, true);
            oEvt.preventDefault();
          },
          'click:prevMonth': function (oEvt) {
            switchMonth.call(this, false);
            oEvt.preventDefault();
          }
        }
      },
      _Init: function () {
        var options = this._Opts;
        var startMonth = getDate(options.startMonth);
        var curentDate = this._today = getDate(options.today) || new Date();
        this._current = {
          selection: [],
          month: null/*,
          monthDates: null*/
        };
        getRange.call(this);
        getLists.call(this);
        this.setMonth(startMonth || curentDate);
      },
      setMonth: function (mDate) {
        var dMonth = getDate(mDate);
        var loopRange = this._Opts.datesRange.loop;
        var currentVals = this._current;
        var datesRange = this._datesRange;
        if (dMonth !== null && oDates.Compare(currentVals.month, dMonth, 'm') !== 0) {
          if (!this._isMonthInRange(dMonth)) {
            if (!(loopRange && (dMonth = oDates.Clone(datesRange[dMonth < datesRange[0] ? 1 : 0])))) {
              return;
            }
          }
          currentVals.month = oDates.Clone(dMonth, '1d' + dayStartFormat);
          drawDates.call(this);
          fillMonthVals.call(this);
        }
      },
      toggleDateSelection: function (mDate) {
        //var currentParams = this._current;
        var dSelect = getDate(mDate);
        if (dSelect !== null && oDates.isInRange(dSelect, this._datesRange)) {
          if (isSelectedDate.call(this, dSelect)) {
            deselectDate.call(this, dSelect);
          } else {
            selectDate.call(this, dSelect);
          }
        }
      },
      _isMonthInRange: function (dMonth) {
        var aRange = this._datesRange;
        return oDates.isInRange(
          oDates.Clone(dMonth, '1d' + dayStartFormat), aRange
        ) || oDates.isInRange(
          oDates.Clone(dMonth, '+1m0d' + dayStartFormat), aRange
        );
      },
      _selectDate: selectDate
    };
  }()), defOptions, null, 'Calendar');
  oBasic.Extend(true, oCpf, {
    Modules: {
      Calendar: Calendar
    }
  });
}(this.jQuery, this, this.ru.mail.cpf));