/**
 * @require 'lib/basic.js'
 * @require 'tools/tools.Images.js',
 * @require 'tools/tools.Deferred.js',
 */
(function (window, oNS) {
	var oBasic = oNS.Basic;
	var typeOf = oBasic.typeOf;
	var fExtend  = oBasic.Extend;
	var isArray = oNS.Types.Array.isArray;
	var oImageTools = oNS.Tools.Images;
	var fDeferred = oNS.Tools.Deferred;
	var Gallery = oBasic.Constructors.getModel((function () {
		//допустимые свойства изображений
		var aDefImageProps = ['title', 'source', 'description'];
		function hOnPreload(elImage) {
			this.loading = true;
			if (elImage) {
				this.width = elImage.width;
				this.height = elImage.height;
			}
		}
		function hOnPreloadFail() {
			delete this.loading;
		}
		/**
		 * Проверяет возможна ли предзагрузка версии изображения и, если возможно и не была выполненна ранее - выполняет ее
		 * @memberof Gallery#
		 * @param {object} oVersion объект версии
		 * @param {number} [iFailTime] значение таймаута загрузки
		 */
		function preloadImageVersion(oVersion, iFailTime) {
			var bCanPreload;
			if (!('loading' in oVersion)) {
				bCanPreload = oImageTools.canPreload(oVersion.type);
				if (bCanPreload) {
					oVersion.loading = oImageTools.preloadImage(oVersion.src, iFailTime).done(hOnPreload.bind(oVersion)).fail(hOnPreloadFail.bind(this));
				} else {
					oVersion.loading = bCanPreload;
				}
			}
			return oVersion.loading;
		}
		function isCurrentImage(imageId, setId) {
			var oCurrent = this._Current;
			return oCurrent.image === imageId && oCurrent.set === setId;
		}
		function preloadNearlyImages(imageId, setId) {
			var iDepth = this._Opts.preloadDepth;
			var aImages, prldImgId;
			//если изменилось текущее изображение - нужно предзагружать уже другой набор
			if (iDepth > 0 && isCurrentImage.call(this, imageId, setId)) {
				aImages = this._getCurrentSetImages();
				for (var dpthImgNo = imageId + iDepth; dpthImgNo >= imageId - iDepth; dpthImgNo--) {
					prldImgId = this._getImageId(dpthImgNo);
					if (prldImgId != imageId) {
						this._startPreload(aImages[prldImgId], prldImgId, setId);
					}
				}
			}
		}
		function hOnImageVersionPreload(versionId, imageId, setId, mPreloadResult) {
			var aImages, bSuccess;
			if (isCurrentImage.call(this, imageId, setId)) {
				bSuccess = !typeOf(mPreloadResult, 'undefined');
				if (bSuccess) {
					aImages = this._getCurrentSetImages();
					this._trigger('imageready', aImages[imageId], imageId, setId, versionId);
				} else {
					this._trigger('imagefail', imageId, setId, versionId);
				}
			}
		}
		/**
		 * Инициирует загрузку версий текущего изображения и, по ее окончанию - загрузку соседних
		 * @param {number} imageId идентификатор текущего изображения
		 * @param {object} oImage объект текущего изображения
		 * @returns {promise} объект для назначения обработчиков результатов предзагрузки
		 */
		function preloadCurImage(imageId, oImage) {
			var setId = this._Current.set;
			var aPreloadPromises = this._startPreload(oImage, imageId, setId);
			for (var versionId = aPreloadPromises.length; versionId--;) {
				aPreloadPromises[versionId].always(hOnImageVersionPreload.bind(this, versionId, imageId, setId));
			}
			return fDeferred.when.apply(null, aPreloadPromises).always(preloadNearlyImages.bind(this, imageId, setId));
		}
		function processImageSets(mImageSets) {
			if (isArray(mImageSets)) {
				for (var setNo = 0 ; setNo < mImageSets.length; setNo++) {
					this.addImagesSet(mImageSets[setNo]);
				}
			} else if (typeOf(mImageSets, 'object')) { //one set
				this.addImagesSet(mImageSets);
			}
		}
		/**
		 * Формирует объект версии изображения
		 * @memberof Gallery#
		 * @param {string} sImageUrl строка с url изображения, для которого (на его основании) формируется обект
		 * может иметь вид <image_url>|<image_width>x<image_height>|<image_type>
		 * @returns {object|null} oParams параметр со свойствами изображения или null, если тип не определен или не поддерживается
		 * @property {string} src url
		 * @property {string} type тип
		 * @property {number} width ширина
		 * @property {number} height высота
		 */
		function getImageParamsFromUrl(sImageUrl) {
			var aImageParams = sImageUrl.split('|');
			var mImageSizes, sType;
			var oParams = {
				src: aImageParams[0],
				type: sType = aImageParams[2] || 'pic'
			};
			if (!(sType && oImageTools.isTypeCorrect(sType))) {
				oParams.type = sType = oImageTools.getImageTypeFromUrl(sImageUrl);
			}
			if (sType) {
				if ((mImageSizes = aImageParams[1]) && (mImageSizes = mImageSizes.split('x')).length > 1) {
					fExtend(oParams, {
						width: mImageSizes[0] * 1,
						height: mImageSizes[1] * 1
					});
				}
			} else {
				oParams = null;
			}
			return oParams;
		}
		/**
		 * Получает набор версий изображения из строк с url
		 * @memberf Gallery#
		 * @param {Array|string} mImage строка или массив строк с url изображений
		 * @returns {Array} aVersions набор объектов версии изображения @see Gallery#getImageParamsFromUrl
		 */
		function getImageVersions(mImage) {
			var aVersions = [];
			var mVersionUrl, mParams;
			if (!isArray(mImage)) {
				mImage = [mImage];
			}
			for (var versionNo = 0; versionNo < mImage.length; versionNo++) {
				if (typeOf(mVersionUrl = mImage[versionNo], 'string') && mVersionUrl.length > 0) {
					if ((mParams = getImageParamsFromUrl(mVersionUrl)) !== null) {
						aVersions.push(mParams);
					}
				}
			}
			return aVersions;
		}
		function processImageSource(oImage) {
			var mSource = oImage.source;
			if (typeOf(mSource, 'string')) {
				oImage.source = mSource = mSource.split('|');
			}
			if (!(isArray(mSource) && mSource[0])) {
				delete oImage.source;
			}
		}
		/**
		 * Формирует набор изображений, проверяет корректность данных
		 * @param {Array} aImages массив исходных данных, для формирования набора изображений
		 * @returns {Array} aResult набор объектов изображений
		 */
		function getImages(aImages/*, setId*/) {
			var aResult = [], oCurImage;
			var mCurImage, mVersions, sPropName;
			var aImageProps = this.getImagePropsList();
			for (var imageNo = 0; imageNo < aImages.length; imageNo++) {
				mCurImage = aImages[imageNo];
				oCurImage = {};
				if (typeOf(mCurImage, 'object')) {
					mVersions = mCurImage.url;
					for (var propNo = aImageProps.length; propNo--;) {
						sPropName = aImageProps[propNo];
						if (mCurImage.hasOwnProperty(sPropName)) {
							oCurImage[sPropName] = mCurImage[sPropName];
						}
					}
					if ('source' in oCurImage) {
						processImageSource(oCurImage);
					}
				} else {
					mVersions = mCurImage;
				}
				mVersions = getImageVersions(mVersions);
				if (mVersions.length) {
					oCurImage.versions = mVersions;
					aResult.push(oCurImage);
				}
			}
			return aResult;
		}
		function isValidId(mId) {
			return mId !== null && !isNaN(mId);
		}
		function isAvailibleId(mId, mSet) {
			return isValidId(mId) && mId in mSet;
		}
		/**
		 * @lends Gallery.prototype
		 */
		return {
			_Events: ['setchange', 'setimageschange', 'imagechange', 'imageready', 'imagefail', 'imageloaded', 'lastimage'], //Memory?
			_Init: function (mImageSets) {
				var sPropName;
				var aAddImageProps = this._Opts.imageProps;
				var aImageProps = this._aImageProps = aDefImageProps.slice(0);
				this._setsParams = [];
				this._setsImages = [];
				this._Current = {
					set: null,
					image: null
				};

				if (isArray(aAddImageProps)) {
					for (var propNo = aAddImageProps.length; propNo--;) {
						sPropName = aAddImageProps[propNo];
						if (aImageProps.indexOf(sPropName) === -1) {
							aImageProps.push(sPropName);
						}
					}
				}
				processImageSets.call(this, mImageSets);
			},
			_getSetImages: function (setId) {
				var aImagesSets = this._setsImages;
				var aImages = null;
				if (setId === null) {
					setId = this._Current.set;
				}
				if (isAvailibleId(setId, aImagesSets)) {
					aImages = this._setsImages[setId];
				}
				return aImages;
			},
			/**
			 * Инициирует предзагрузку версий изображения
			 * @memberof Gallery#
			 * @param {object} oImage  Объект изображения
			 * @param {number} imageId  Идентификатор изображения
			 * @param {number} setId  Идентификатор набора изображений
			 */
			_startPreload: function (oImage, imageId, setId) {
				var iFailTime = this._Opts.preloadTimeout;
				var aImageVersions = oImage.versions;
				var aPromises = [], oPromise, mPreloadResult, oCurVersion, bNotPreloaded;
				for (var versionId = 0; versionId < aImageVersions.length; versionId++) {
					oCurVersion = aImageVersions[versionId];
					bNotPreloaded = !('loading' in oCurVersion);
					mPreloadResult = preloadImageVersion(oCurVersion, iFailTime);
					oPromise = fDeferred.when(mPreloadResult);
					if (bNotPreloaded) {
						oPromise.done(this._trigger.bind(this, 'imageloaded', oImage, imageId, setId, versionId));
					}
					aPromises.push(oPromise);
				}
				return aPromises;
			},
			/**
			 * "Округляет" порядковый номер изображения, в рамках текущего набора
			 * @param {number} iNumber
			 */
			_getImageId: function (iNumber) {
				var aSetImages = this._getCurrentSetImages();
				var iLastImgId = (aSetImages && aSetImages.length || 0) - 1;
				var iLength;
				if (this._Opts.Loop === true) {
					iLength = iLastImgId + 1;
					iNumber = iNumber % iLength;
					if (iNumber < 0) {
						iNumber = iNumber + iLength;
					}
				} else {
					iNumber = Math.max(0, Math.min(iNumber, iLastImgId));
				}
				return iNumber;
			},
			_getCurrentSetImages: function () {
				return this._setsImages[this._Current.set];
			},
			addImagesSet: function (oSetParams) {
				var aSetsParams = this._setsParams;
				var aSetsImages = this._setsImages;
				var aSetImages, setId = -1;
				if (typeOf(oSetParams, 'object') && isArray(aSetImages = oSetParams.images)) {
					setId = aSetsImages.length;
					aSetImages = getImages.call(this, aSetImages, setId);
					if (aSetImages.length) {
						aSetsImages.push(aSetImages);
						delete oSetParams.images;
						aSetsParams[setId] = oSetParams;
					} else {
						setId = -1;
					}
				}
				return setId;
			},
			/**
			 * Добавляет изображения к текущему набору
			 * @memberof #Gallery
			 * @param {Array} aImages массив изображений (строки/массивы строк с url или объекты с параметрами)
			 * @param {number} [setId]  Идентификатор набора
			 * @param {boolean} [bPrepend] флаг, указывающий нужно ли добавить изображения в начало набора
			 */
			addImages: function (aImages, setId, bPrepend) {
				var aSetsImages = this._setsImages;
				setId = isAvailibleId(setId, aSetsImages) ? setId : this._Current.set;
				var aSetImages = aSetsImages[setId];
				if (isArray(aImages)) {
					aImages = getImages.call(this, aImages, setId);
					if (aImages.length) {
						Array.prototype[bPrepend ? 'unshift' : 'push'].apply(aSetImages, aImages);
						this._trigger('setimageschange', aImages, setId);
					}
				}
			},
			/**
			 * Устанавливает текущий набор изображений
			 * @param {number} setId идентификатор набора
			 * @returns {boolean} bSetChenged признак изменения набора, если набор с переданным id существует - истина
			 */
			changeSet: function (setId) {
				var oCurent = this._Current;
				var bSetChenged = isAvailibleId(setId, this._setsImages) && setId != oCurent.set;
				if (bSetChenged) {
					oCurent.set = setId * 1;
					oCurent.image = null;
					this._trigger('setchange', this._setsParams[setId], setId);
				}
				return bSetChenged;
			},
			/**
			 * Переключает изображения в рамках текущего набора
			 * @param {number} imageId идентификатор изображения
			 * @returns {boolean} bImageChanged признак смены изображения
			 */
			changeImage: function (imageId) {
				var oCurent = this._Current;
				var aImages = this._getCurrentSetImages();
				var bImageChanged = false, oCurImage, bIsLast, bIsFirst;
				if (aImages && isValidId(imageId)) {
					imageId = this._getImageId(imageId);
					bImageChanged = imageId != oCurent.image;
					if (bImageChanged) {
						oCurent.image = imageId * 1;
						oCurImage = aImages[imageId];
						this._trigger('imagechange', oCurImage, imageId, oCurent.set);
						if (this._Opts.Loop !== true) {
							bIsLast = imageId === aImages.length - 1;
							bIsFirst = imageId === 0;
							if (bIsLast || bIsFirst) {
								this._trigger('lastimage', bIsLast, bIsFirst);
							}
						}
						preloadCurImage.call(this, imageId, oCurImage);
					}
				}
				return bImageChanged;
			},
			nextImage: function () {
				return this.changeImage(this._Current.image + 1);
			},
			prevImage: function () {
				return this.changeImage(this._Current.image - 1);
			},
			getImagePropsList: function () {
				//возвращаем копию массиива, чтобы исключить возможность его внешней модификации
				return this._aImageProps.slice(0);
			},
			getCurrentImage: function () {
				var oCurrent = this._Current;
				var aSetImages = this._getCurrentSetImages();
				return {
					image: oCurrent.image,
					set: oCurrent.set,
					setLength: aSetImages && aSetImages.length || 0
				};
			},
			/**
			 * Получает набор свойств произвольного изображения
			 * @param {number} [imageId] идентификатор изображения (если не указан, будет использован текущий)
			 * @param {number} [setId] идентификатор набора (если не указан, будет использован текущий)
			 */
			getImageProps: function (imageId, setId) {
				var oCurrent = this._Current;
				var aSetsImages = this._setsImages;
				var oProps = null;
				imageId = isValidId(imageId) ? imageId : oCurrent.image;
				setId = isValidId(setId) ? setId : oCurrent.set;
				if (isAvailibleId(setId, aSetsImages) && isAvailibleId(imageId, aSetsImages[setId])) {
					oProps = fExtend(true, {}, aSetsImages[setId][imageId]);
					delete oProps.versions;
				}
				return oProps;
			},
			isImageReady: function (imageId, setId, versionId) {
				var aSetsImages = this._setsImages;
				var oCurrent = this._Current;
				var aSetImages, oImage, bReady = false;
				if (!isAvailibleId(setId, aSetsImages)) {
					setId = oCurrent.set;
					if (setId === null) {
						return false;
					}
				}
				aSetImages = aSetsImages[setId];
				if (!isAvailibleId(imageId, aSetImages)) {
					imageId = oCurrent.image;
				}
				oImage = aSetImages[imageId];
				if (oImage && isAvailibleId(versionId, oImage.versions)) {
					bReady = typeOf(oImage.versions[versionId].loading, 'boolean');
				}
				return bReady;
			},
			getSetParams: function (setId) {
				var oSetsParams = this._setsParams;
				var oResult = null;
				if (!isNaN(setId) && setId !== null && setId in oSetsParams) {
					oResult = fExtend(true, {}, oSetsParams[setId]);
				}
				return oResult;
			}
		};
	}()), {
		_Handlers: null,
		preloadDepth: null,
		preloadTimeout: 2e4,
		Loop: false,
		imageProps: null
	}, null, 'Gallery');

	fExtend(window.getNameSpace('Modules', oNS), {
		Gallery: Gallery
	});
}(this, this.ru.mail.cpf));